<?php

/**
 * Adds an item in the library array.
 */
function scald_efq_add_item(&$library, $sid) {
  $atom = scald_fetch($sid);
  $context = variable_get('dnd_context_default', 'sdl_editor_representation');
  $library['atoms'][$sid] = array(
    'meta' => array(
      'title'  => $atom->title,
      'type'   => $atom->type,
      'legend' => '',
    ),
    'sas'     =>  '[scald=' . $atom->sid . ':' . $context .']',
    'editor'  => scald_render($atom, $context),
    'preview' => scald_render($atom, 'scald_efq_preview'),
  );

  // theme_scald_efq_editor_legend() requires a rendered atom.
  // We call it only here to make sure that $atom is rendered.
  if (empty($atom->omit_legend)) {
    $library['atoms'][$sid]['meta']['legend'] = theme('scald_efq_editor_legend', array('atom' => $atom));
  }
}

/**
 * Return a JSON representation of a Library Page.
 */
function scald_efq_library_json_callback() {

  $library = array();
  $summary = array('criteria' => array());
  $sortDef = array(
    array(
      'asc'  => t("ascending"),
      'desc' => t("descending")
    ),
    array(
      'sid'  => t("creation"),
      'type' => t("type"),
    ),
  );

  $q = new EntityFieldQuery();
  $q->entityCondition('entity_type', 'scald_atom');
  $q->pager(10);

  // Sorting.
  // Paratemers from URL.
  if (!empty($_GET['order'])) {
    $sortOrder = ('desc' === $_GET['order']) ? 'desc' : 'asc';
  } else {
    $sortOrder = 'asc';
  }
  if (!empty($_GET['sort'])) {
    switch ($_GET['sort']) {
      case 'sid':
      case 'type':
        $sortColumn = $_GET['sort'];
        break;
      default:
        $sortColumn = 'sid';
        break;
    }
  } else {
    $sortColumn = 'sid';
  }
  // Label for library display.
  $summary['sort'] = t('<span class="label">Sort:</span> @criteria', array(
    '@criteria' => $sortDef[1][$sortColumn] . ' ' . $sortDef[0][$sortOrder]
  ));
  // Query.
  $q->propertyOrderBy($sortColumn, $sortOrder);

  // Filtering.
  if (!empty($_GET['type'])) {
    $q->propertyCondition('type', $_GET['type']);
  }
  if (!empty($_GET['title'])) {
    $q->propertyCondition('title', '%' . db_like($_GET['title']) . '%', 'LIKE');
  }
  if (!empty($_GET['language'])) {
    $q->propertyCondition('language', $_GET['language']);
  }
  if (!empty($_GET['author'])) {
    $q->fieldCondition('scald_authors', 'tid', $_GET['author']);
  }
  if (!empty($_GET['tag'])) {
    $q->fieldCondition('scald_tags', 'tid', $_GET['tag']);
  }

  // Sorting.
  /*
   * This is my code, and this is not working...
   *
  foreach ($sortDef[1] as $type => $label) {
    $summary['criteria'][] = l(t("Sort by @criteria", array('@criteria' => $label)), current_path(), array(
      'query' => array(
        'sort'  => $type,
        'order' => $sortOrder,
      ) + drupal_get_query_parameters(),
    ));
  }
   */

  // Render our header based on the built summary.
  $header = '<div class="summary">';
  $header .= '<div class="toggle"></div><div class="title">' . t('search') . '</div>';
  if (!empty($summary['sort'])) {
    $header .= '<div class="sort">' . $summary['sort'] . '</div>';
  }
  $header .= theme('item_list', array('items' => $summary['criteria']));
  $header .= '</div>';

  // Load and register atoms.
  $r = $q->execute();
  $output = array();
  if (!empty($r['scald_atom'])) {
    foreach (scald_atom_load_multiple(array_keys($r['scald_atom'])) as $atom) {
      // Serious huge WTF.
      scald_efq_add_item($library, $atom->sid);
      $output[$atom->sid]['#markup'] = scald_render($atom->sid, 'scald_efq_library_item');
      $output[$atom->sid]['#prefix'] = '<div class="editor-item clearfix" id="sdl-' . $atom->sid . '">';
      $output[$atom->sid]['#suffix'] = '</div>';
    }
  }

  // Why would the fuck I would want to display this here?
  // But hey let's keep it.
  $library['library'] = array(
    'messages' => array(
      '#markup' => theme('status_messages'),
    ),
    'search' => array(
      // Seriously Scald, why?
      '#prefix' => '<div class="view-filters">',
      '#suffix' => '</div>',
      'form' => drupal_get_form('scald_efq_library_form', $_GET),
    ),
    'atoms' => $output,
    'header' => array(
      '#markup' => $header,
    ),
    'footer' => array(
      'pager' => array(
        '#theme' => 'pager'
      ),
    ),
  );

  // Prepare the "Quick add" buttons, that will appear next to the library,
  // based on the user permissions.
  $atom_types = scald_types();
  $buttons = array('type' => 'ul', 'title' => null, 'attributes' => array());
  ctools_include('ajax');
  ctools_include('modal');
  foreach ($atom_types as $type) {
    if (scald_action_permitted(new ScaldAtom($type->type), 'create')) {
      $text = t($type->type);
      $alt = t('Create a new !type atom', array('!type' => $text));
      $buttons['items'][] = array(
        'data'  => ctools_modal_text_button($text, 'atom/add/' . $type->type . '/nojs', $alt, 'ctools-modal-custom-style'),
        'class' => array('add-' . drupal_strtolower($type->type)),
      );
    }
  }

  $library['menu']    = '<div class="scald-menu"><div class="add-buttons">' . theme('item_list', $buttons) . '</div></div>';
  $library['library'] = '<div class="scald-library">' . render($library['library']) . '</div>';
  $library['anchor']  = '<div class="scald-anchor"></div>';

  drupal_json_output($library);
}

/**
 * Library search form.
 */
function scald_efq_library_form($form, &$form_state, array $filters = array()) {

  $defaults = $filters + array(
    'author'   => null,
    'language' => null,
    'tag'      => null,
    'title'    => null,
    'type'     => null,
  );

  $form = array(
    '#method'      => 'GET',
    '#token'       => false,
    '#after_build' => array('scald_efq_library_form_after_build'),
  );
  $form_state['method'] = 'GET';

  $form['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title contains'),
    '#default_value' => $defaults['title'],
  );

  // Author.
  if ($vid = variable_get('scald_efq_author_vocabulary', 0)) {
    $options = db_query("SELECT tid, name FROM {taxonomy_term_data} WHERE vid = :vid ORDER BY name ASC", array(':vid' => $vid))->fetchAllKeyed();
    if (0 < count($options)) {
      $form['author'] = array(
        '#type'          => 'select',
        '#title'         => t("Author"),
        '#options'       => array(null => t("All")) + $options,
        '#default_value' => $defaults['author'],
      );
    }
  }

  // Keywords.
  if ($vid = variable_get('scald_efq_tags_vocabulary', 0)) {
    $options = db_query("SELECT tid, name FROM {taxonomy_term_data} WHERE vid = :vid ORDER BY name ASC", array(':vid' => $vid))->fetchAllKeyed();
    if (0 < count($options)) {
      $form['tag'] = array(
        '#type'          => 'select',
        '#title'         => t("Tag"),
        '#options'       => array(null => t("All")) + $options,
        '#default_value' => $defaults['author'],
      );
    }
  }

  // Atom types.
  $options = db_query("SELECT type, COUNT(type) AS count FROM {scald_atoms} GROUP BY type")->fetchAllKeyed();
  if (1 < count($options)) { // Do not display when useless.
    $reference = scald_types();
    foreach ($options as $type => $count) {
      $countLabel = " (" . $count . ")";
      if (isset($reference[$type])) {
        $options[$type] = t($reference[$type]->title) . $countLabel;
      } else {
        $options[$type] = $type . $countLabel;
      }
    }
    asort($options);
    $form['type'] = array(
      '#type'          => 'checkboxes',
      '#options'       => $options,
      '#title'         => t('Atom type'),
      '#default_value' => $defaults['type'],
    );
  }

  // Language types.
  $options = db_query("SELECT language, COUNT(language) AS count FROM {scald_atoms} GROUP BY language")->fetchAllKeyed();
  if (1 < count($options)) { // Do not display when useless.
    require_once DRUPAL_ROOT . '/includes/iso.inc';
    $reference = _locale_get_predefined_list();
    foreach ($options as $langcode => $count) {
      $countLabel = " (" . $count . ")";
      if (isset($reference[$langcode])) {
        $options[$langcode] = t($reference[$langcode][0]) . $countLabel;
      } else if (empty($langcode) || LANGUAGE_NONE === $langcode) {
        $options[$langcode] = '-- ' . t("Undefined") . $countLabel;
      } else {
        $options[$langcode] = $langcode . $countLabel;
      }
    }
    asort($options);
    $form['language'] = array(
      '#type'          => 'checkboxes',
      '#options'       => $options,
      '#title'         => t("Language"),
      '#default_value' => $defaults['language'],
    );
  }

  $form['filter-submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Search')
  );

  return $form;
}

/**
 * After build callback on the form element to remove form build ID.
 * It allow us to cache per page (as form build id is supposed to be unique).
 */
function scald_efq_library_form_after_build(&$element, &$form_state) {
  unset($element['form_build_id']);
  unset($element['form_id']);
  return $element;
}

/**
 * Returns HTML for an atom rendered in the "Library Item" context.
 */
function theme_scald_efq_library_item($variables) {

  $atom  = $variables['atom'];
  $image = $variables['image'];
  $infos = $atom->rendered;

  // Action links.
  $links = scald_atom_user_build_actions_links($atom, null);
  // Force all links to open in a new window.
  foreach ($links as $action => $link) {
    $links[$action]['attributes']['target'] = '_blank';
  }
  // The Insert link. Use the "_" prefix to avoid collision with possible
  // "insert" action.
  $links['_insert'] = array(
    'title'      => t('Insert'),
    'attributes' => array('data-atom-id' => $atom->sid),
    'href'       => '#',
  );
  $links = array(
    '#theme'      => 'links',
    '#links'      => $links,
    '#attributes' => array('class' => array('links', 'inline')),
  );
  $links = drupal_render($links);

  // Authors.
  $names = array();
  if (!empty($infos->authors)) {
    foreach ($infos->authors as $author) {
      $names[] = check_plain($author->name);
    }
  }
  $authors = implode(', ', $names);
  $class = drupal_html_class($atom->type);

  return <<<EOT
<div class='image'>{$image}</div>
<div class='meta type-{$class} clearfix'>
  <div class='title'>{$infos->title}</div>
  <div class='author'>{$authors}</div>
  {$links}
</div>
EOT;
}

/**
 * Returns HTML for an atom rendered in the "Editor Representation" context.
 */
function theme_scald_efq_editor_item($variables) {
  if (empty($variables['informations']->player)) {
    return $variables['image'];
  } else {
    $player = $variables['informations']->player;
    $output = is_array($player) ? $player : array('#markup' => $player);
    $output += array(
      '#prefix' => '<div class=\'image\'>',
      '#suffix' => '</div>',
    );
    return drupal_render($output);
  }
}

/**
 * Returns HTML for the legend of an atom.
 */
function theme_scald_efq_editor_legend($variables) {
  $atom = $variables['atom'];
  if (!empty($atom->rendered->authors)) {
    foreach ($atom->rendered->authors as $author) {
      $links[] = $author->link;
    }
    $by = implode(', ', $links);
  }
  else {
    $by = $atom->rendered->publisher['link'];
  }
  $by = t('by !name', array('!name' => $by));
  return  <<<EOT
<div class='meta'>
  <!--copyright={$atom->sid}-->{$atom->rendered->title}, {$by}<!--END copyright={$atom->sid}-->
</div>
EOT;
}

/**
 * Returns HTML for an atom rendered in the "Preview" context.
 */
function theme_scald_efq_preview($variables) {

  $atom               = $variables['atom'];
  $image              = $variables['image'];
  $resource_label     = t('Resource');
  $informations_label = t('Informations');
  $resource           = theme('scald_efq_editor_item', array('informations' => $atom->rendered, 'image' => $image));
  $title_label        = t('Title');
  $title_value        = $atom->rendered->title;

  if (!empty($atom->rendered->authors)) {
    $author_label = t('Author');
    foreach ($atom->rendered->authors as $author) {
      $names[] = $author->link;
    }
    $author_name = implode(', ', $names);
    $author = "<dt>$author_label</dt><dd>$author_name</dd>";
  } else {
    $author = "";
  }

  return <<<EOT
<div class='sdl-preview-item'>
  <h3>{$resource_label}</h3>
  {$resource}
  <h3>{$informations_label}</h3>
  <dl>
    <dt>{$title_label}</dt>
    <dd>{$title_value}</dd>
    {$author}
  </dl>
</div>
EOT;
}
